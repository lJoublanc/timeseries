name := "timeseries"

description := "A pure data-structure for time-series"

organization := "com.dinogroup"

//crossScalaVersions := Seq("2.12.8", "2.13.0-M5")

scalaVersion := "2.13.0-M5"

enablePlugins(GitVersioning)

licenses += "MIT" -> url("http://opensource.org/licenses/MIT")

publishMavenStyle := false

bintrayRepository := "dinogroup"

bintrayOrganization := Some("dmbl")


scalacOptions ++= Seq(
    "-feature",
    "-deprecation",
    "-language:implicitConversions",
    "-language:higherKinds",
    "-language:postfixOps",
    "-language:reflectiveCalls",
    "-language:existentials")

resolvers += Resolver.sonatypeRepo("releases")

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.9")

lazy val catsVer = "1.5.0"

resolvers += Resolver.bintrayRepo("dmbl","dinogroup") //needed for fs2-columns

libraryDependencies ++= Seq(
  "com.dinogroup" %% "fs2-columns" % "0.1.3",
  "co.fs2" %% "fs2-core" % "1.0.3",
  "org.scalacheck" %% "scalacheck" % "1.14.0",
  "org.scalatest" %% "scalatest" % "3.0.6-SNAP5" % "test",
  "org.typelevel" %% "cats-core" % catsVer,
  "org.typelevel" %% "cats-laws" % catsVer % "test",
  "org.typelevel" %% "cats-testkit" % catsVer % "test"
)

initialCommands in console := """
  import timeseries._
  import timeseries.implicits._
  import fs2.Stream
  import cats.effect.IO
"""

initialCommands in consoleQuick := """
import fs2._
import cats._
import cats.syntax.all._
import cats.instances.all._
import cats.implicits._
import cats.effect._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
"""

scalafmtOnCompile := true

/* The below is needed for CI on Gitlab/Kubernetes.
 * As this is a library, it is not mean to be run independently,
 * therefore this is a null task.
 */

val stage = taskKey[Unit]("Stage task")

val Stage = config("stage")

stage := { }
