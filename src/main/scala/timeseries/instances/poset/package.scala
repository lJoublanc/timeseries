package timeseries.instances

import fs2._
import cats._
import cats.arrow.Arrow
import cats.syntax.all._
import cats.effect._
import cats.instances.either._
import org.scalacheck.Arbitrary

import scala.reflect.ClassTag
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration
import _root_.timeseries._
import timeseries.timestamp._

package object poset {

  /** Returns an `F[Arbitrary[TimeSeries]]` with timestamps around now.
    * The timestamps are relative to the current time, which is determined upon
    * evaluation of `F`.  The timestamps are chosen to have a short consecutive
    * time, to make this parctical for generating time-series for testing.
    */
  implicit def arbitraryTimeSeries[
      F[_]: Async,
      τ <: TimeUnit: TimestampConversion,
      V: Arbitrary
  ](implicit
    ec: ExecutionContext,
    timer: Timer[F],
    order: Order[Timestamp[τ]]): F[Arbitrary[TimeSeries[Pure, τ, Nothing, V]]] =
    Timestamp[τ].fromTimer[F] map { now =>
      import org.scalacheck.Gen._
      import org.scalacheck.Arbitrary._

      Arbitrary {
        import scala.concurrent.duration._
        for {
          deltas <- listOf((Timestamp[τ].toJavaTimeUnit match {
            case NANOSECONDS  => choose[Long](0, 1000000000)
            case MICROSECONDS => choose[Long](0, 3000000)
            case MILLISECONDS => choose[Long](0, 1000)
            case SECONDS      => choose[Long](0, 3)
            case MINUTES      => choose[Long](0, 1)
            case _ =>
              throw new IllegalArgumentException(
                "Refusing to create arbitrary time-series greater than minutes"
              )
          }) map Timestamp[τ])
          index = deltas
            .scanLeft(now)((a, b) => Timestamp[τ](a.toLong + b.toLong))
            .tail
          values <- listOfN[V](index.size, arbitrary[V])
        } yield Series(index, values)
      }
    }
}
