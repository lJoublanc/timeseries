package timeseries.instances

import _root_.timeseries.{Series, =>>, MonotoneFn, Index}
import cats.{Functor => _, _}
import cats.arrow._
import cats.effect.Sync
import cats.instances.function._
import cats.instances.tuple._
import cats.syntax.all._
import fs2._
import fs2.columns._
import fs2.Pull.syncInstance
import org.scalacheck.Arbitrary

/** The 'pure' name implies no <em>side</em>-effects (although effects that maintain
  * referential transparency are fine).
  */
package object pure {

  implicit def tupValues[A, B](implicit A: ValueOf[A],
                               B: ValueOf[B]): ValueOf[(A, B)] =
    new ValueOf(valueOf[A] -> valueOf[B])

  implicit def showPureLabelled[L, K, V](
      implicit labels: ValueOf[L],
      showLabels: Show[L],
      showData: Show[(K, V)]): Show[Series[Pure, K, L, V]] =
    _.toStream.toVector match {
      case v if v.isEmpty => "Series(empty)"
      case v =>
        "| index | " + valueOf[L].show + " |" + "\n" + //TODO: somehow align with data.
          v.map("| " + _.show + " |").mkString("\n")
    }

  implicit def showPureUnlabelled[K, V](
      implicit showData: Show[(K, V)]): Show[Series[Pure, K, Nothing, V]] =
    _.toStream.toVector match {
      case v if v.isEmpty => "Series(empty)"
      case v              => v.map("| " + _.show + " |").mkString("\n")
    }

  /** Compare two pure series. A `Series` is:
    * <ul>
    *   <li/> equal if all it's keys and values are the same.
    *   <li/> smaller if it's keys, when aligned, are all smaller than the other's.
    *   <li/> uncomparable otherwise.
    * </ul>
    * FIXME : optimize this for ordered/strict, and use a pull to do this for effects.
    * Two states : first state, zip and compare to see if equality. As soon as they're different,
    * switch to lt/gt mode.
    */
  implicit def partialOrder[L, K: Order, V: Order]
    : PartialOrder[Series[Pure, K, L, V]] =
    (a: Series[Pure, K, L, V], b: Series[Pure, K, L, V]) => {

      implicit val cmpTup: PartialOrder[(K, V)] = (a: (K, V), b: (K, V)) =>
        if (a == null || b == null) Double.NaN //set below
        else if (a._1 < b._1) -1.0
        else if (a._1 > b._1) +1.0
        else if (a._2 === b._2) 0.0
        else Double.NaN

      def cmpSorted(a: Stream[Pure, (K, V)], b: Stream[Pure, (K, V)]): Double =
        (a zipAllWith b)(null, null)((a, b) => cmpTup partialCompare (a, b)) //FIXME
        .zipWithPrevious
          .takeWhile({
            case (Some(a), b) => a == b
            case (None, b)    => true
          }, takeFailure = true)
          .lastOr(None -> 0.0)
          .toVector
          .headOption match {
          case Some((Some(a), b)) => if (a == b) b else Double.NaN
          case Some((None, b))    => b
          case None               => 0.0
        }

      if (a.isOrdered && b.isOrdered) cmpSorted(a.toStream, b.toStream)
      else
        cmpSorted(
          Stream emits a.toStream.toVector.sortBy(_._1)(Order[K].toOrdering),
          Stream emits b.toStream.toVector.sortBy(_._1)(Order[K].toOrdering)
        )
    }

  implicit def catsStdFlatMapForIndex[F[_], L, V]: FlatMap[Series[F, ?, L, V]] =
    new FlatMap[Series[F, ?, L, V]] {

      def map[A, B](fa: Series[F, A, L, V])(f: A => B): Series[F, B, L, V] = {
        val toStream = fa.toStream mapChunks { case a |: v => a.map(f) |: v }

        f match {
          case f: MonotoneFn[A, _] =>
            new Series(toStream, fa.isUnique && f.isStrict, fa.isOrdered)
          case f: Function1[A, _] =>
            new Series(toStream, isUnique = false, isOrdered = false)
        }
      }

      def flatMap[A, B](fa: Series[F, A, L, V])(
          f: A => Series[F, B, L, V]): Series[F, B, L, V] = {
        val toStream = fa.toStream flatMap { case (a, v) => f(a).toStream }
        f match {
          case f: MonotoneFn[A, _] =>
            new Series(toStream, fa.isUnique && f.isStrict, fa.isOrdered)
          case f: Function1[A, _] =>
            new Series(toStream, isUnique = false, isOrdered = false)
        }
      }

      def tailRecM[A, B](a: A)(
          f: A => Series[F, Either[A, B], L, V]): Series[F, B, L, V] = {
        val strB: Stream[F, (B, V)] = f(a).toStream.flatMap {
          case (Left(a), v)  => tailRecM(a)(f).toStream
          case (Right(b), v) => Stream(b -> v)
        }
        f match {
          case f: MonotoneFn[A, _] =>
            new Series(strB, isUnique = f.isStrict, isOrdered = true)
          case f: Function1[A, _] =>
            false -> false
            new Series(strB, isUnique = false, isOrdered = false)
        }
      }
    }

  protected trait Functor[F[_], K]
      extends cats.Functor[Series[F, K, Nothing, ?]] {
    def map[A, B](fa: Series[F, K, Nothing, A])(
        f: A => B): Series[F, K, Nothing, B] = fa.map(f).unlabelled
  }

  /** `cats.Functor` on the series values.
    * @note This is provided separately from `Apply` because it doesn't
    *       require the type `K` to have a natural order.
    */
  def catsStdFunctorForValues[F[_], K]: cats.Functor[Series[F, K, Nothing, ?]] =
    new Functor[F, K] {}

  /** `cats.Apply` for series values. Requires the keys to have a natural order. */
  implicit def catsStdApplyForValues[F[_], K: Order](
      implicit U: timeseries.Series.Union.Aux[Nothing, Nothing, Nothing])
    : Apply[Series[F, K, Nothing, ?]] =
    new Apply[Series[F, K, Nothing, ?]] with Functor[F, K] {

      /** Zip series together, yielding the set intersection of their keys. */
      override def product[A, B](
          fa: Series[F, K, Nothing, A],
          fb: Series[F, K, Nothing, B]): Series[F, K, Nothing, (A, B)] =
        fa |: fb

      def ap[A, B](ff: Series[F, K, Nothing, A => B])(
          fa: Series[F, K, Nothing, A]): Series[F, K, Nothing, B] = {
        val fb = (ff product fa).toStream
          .mapChunks {
            case k |: f |: a =>
              k |: Chunk.seq(for (i <- 0 until k.size) yield f(i)(a(i)))
          }
        new Series(fb, ff.isUnique, ff.isOrdered)
      }
    }

  implicit def arbitraryInstance[K: Arbitrary: Order, V: Arbitrary] =
    Arbitrary[Series[Pure, K, Nothing, V]] {
      import org.scalacheck.Gen._
      import org.scalacheck.Arbitrary._

      for {
        ks <- listOf(arbitrary[K])
        vs <- listOfN(ks.size, arbitrary[V])
      } yield Series(ks, vs)
    }
}
