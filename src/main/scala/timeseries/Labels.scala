package timeseries

import fs2.Chunk
import fs2.columns._

import scala.annotation.implicitNotFound

/** Typeclasses for managing serie's labels. */
protected[timeseries] trait Labels {
  //TODO support unaligned labels. i.e. make this an aligner (O(n!)?)
  /** Typeclass to derive the column types `VOut` when selecting a subset.
    * Note that the labels returned are known to be `M`, so no need to derive
    * this.
    * @tparam M (ordered) subset of labels in `L` to extract.
    * @tparam F The effect of the series.
    * @tparam L The labels of the series.
    * @tparam K The keys(s) of the series.
    * @tparam V The value(s) of the series.
    */
  @implicitNotFound("${M} is not an ordered subset of the series' labels ${L}.")
  trait Subset[M, L, K, V] {

    /** The value types (i.e. `V`s) of M. */
    type VOut
    def apply(f: Chunk[(K, V)]): Chunk[(K, VOut)]
  }

  trait LowestPrioSubset {

    implicit def oneLeftMatchingOneRight[L, K, V]: Subset.Aux[L, L, K, V, V] =
      new Subset[L, L, K, V] {
        type VOut = V
        def apply(f: Chunk[(K, V)]): Chunk[(K, V)] = f
      }

    implicit def oneNotMatchingLeftManyRight[M, LH, LT, K, VH, VT]( //LH != M
        implicit tail: Subset[M, LT, K, VT])
      : Subset.Aux[M, (LH, LT), K, (VH, VT), tail.VOut] =
      new Subset[M, (LH, LT), K, (VH, VT)] {
        type VOut = tail.VOut
        def apply(f: Chunk[(K, (VH, VT))]): Chunk[(K, tail.VOut)] =
          tail(f match { case k |: vh |: vt => k |: vt })
      }

  }

  trait LowPrioSubset {

    implicit def oneMatchingLeftManyRight[LH, LT, K, VH, VT]
      : Subset.Aux[LH, (LH, LT), K, (VH, VT), VH] =
      new Subset[LH, (LH, LT), K, (VH, VT)] {
        type VOut = VH
        def apply(f: Chunk[(K, (VH, VT))]): Chunk[(K, VH)] =
          f match { case k |: vh |: _ => k |: vh }
      }
  }

  object Subset extends LowestPrioSubset with LowPrioSubset {

    type Aux[M, L, K, V, MV] = Subset[M, L, K, V] { type VOut = MV }

    implicit def manyMatchingLeftManyRight[MT, LH, LT, K, VH, VT](
        implicit tail: Subset[MT, LT, K, VT])
      : Subset.Aux[(LH, MT), (LH, LT), K, (VH, VT), (VH, tail.VOut)] =
      new Subset[(LH, MT), (LH, LT), K, (VH, VT)] {
        type VOut = (VH, tail.VOut)
        def apply(f: Chunk[(K, (VH, VT))]): Chunk[(K, (VH, tail.VOut))] =
          f match {
            case k |: vh |: vt =>
              tail(k |: vt) match {
                case k |: vout =>
                  k |: vh |: vout
              }
          }
      }
  }

  trait Union[-M, -L] {
    type Out
  }

  trait LowPrioUnion {
    implicit def labelled[LH <: Singleton, LT]: Union.Aux[LH, LT, (LH, LT)] =
      new Union[LH, LT] { type Out = (LH, LT) }
  }

  object Union extends LowPrioUnion {

    type Aux[LH, LT, L] = Union[LH, LT] { type Out = L }

    implicit val unlabelled: Union.Aux[Nothing, Nothing, Nothing] =
      new Union[Nothing, Nothing] { type Out = Nothing }
  }
}
