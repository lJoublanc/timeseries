package timeseries

import cats._
import cats.effect._
import cats.syntax.all._
import fs2._
import fs2.columns._
import org.scalacheck.{Arbitrary, Gen, rng}
import timeseries.timestamp.{Timestamp, TimeUnit, TimestampConversion}

import scala.collection.immutable.{Seq, List, IndexedSeq, ArraySeq, Vector}

/** A map from `K`eys to `V`alues.
  *
  * =Feature Overview=
  *
  *  - Type-safe i.e. can't add series with different type keys and/or values.
  *  - Type-level `L`abels (≥ scala 2.13 required).
  *  - Supports asynchronous effects. i.e. streaming data.
  *  - Vectorised: Underlying data is stored column-wise using `fs2-columns`.
  *    Many operations preserve columns (notable exception: `map`).
  *  - Set and algebraic operations. //TODO
  *
  * =Series Laws=
  *
  *  - Series is a `cats.Bifunctor`. //TODO
  *  - The values of a series form a `cats.Apply` functor over set intersection.
  * TODO should this be 'Union/outer join'?.
  *  - The index of an *ordered* series form a `cats.flatMap` on `MonotonicFn` functions.
  *  - Series are foldable (but don't implement `cats.Foldable` as `fold` return type is
  *    wrapped in the effect).
  *
  * @param toStream Converts this `Series` to a `fs2.Stream`. `O(1)`.
  * @param isUnique True if the ''index'' is unique. `O(1)`.
  *                 Note this is not a bijection.
  * @param isOrdered True if the ''index'' is ordered. `O(1)`.
  *                  Note this is not a bijection.
  * @tparam F The effect type when converted `toStream`.
  * @tparam K The row label type, e.g. `Int`, but possibly a right-nested tuple for multi-index.
  * @tparam L The column label(s), a singleton type e.g. `"celcius"`. Can be `Nothing`.
  * @tparam V The value type, e.g. `Double`, but possibly a right-nested tuple for multi-variate series.
  */
class Series[F[_], K, L, V](val toStream: Stream[F, (K, V)],
                            val isUnique: Boolean,
                            val isOrdered: Boolean) {
  self =>

  /** Override labels with `L`.
    * @example {{{
    * Series(0 until 3, 1 to 3).labelled["fortran"].show
    * }}}
    */
  def labelled[L]: Series[F, K, L, V] =
    self
      .asInstanceOf[Series[F, K, L, V]] //TODO add typeclass that checks arity of labels and matches to columns

  /** Select subset of columns with labels `L2`.  Note the labels must be in the
    * same order as in the series. Also, don't forget to select the index!
    * @example {{{
    * x.show
    * ...
    * x[("author", "publisher")].show
    * res1>
    * |index| author| publisher|
    * }}}
    */
  def apply[L2](implicit S: Series.Subset[L2, L, K, V],
                L: ValueOf[L2]): Series[F, K, L2, S.VOut] =
    new Series[F, K, L2, S.VOut](toStream.mapChunks(kv => S(kv)),
                                 isUnique,
                                 isOrdered)

  /** Drop the labels. Any subsequent operations will not check that the
    * labels between two series match (although data types must still match).
    */
  def unlabelled: Series[F, K, Nothing, V] =
    self.asInstanceOf[Series[F, K, Nothing, V]]

  /** True if the ''index'' is both ordered and unique.
    * i.e. strictly increasing. `O(1)`.
    */
  def isStrictMonotone: Boolean = isOrdered && isUnique

  /*
  /** Filters the index by `key`. */
  def apply(key: K)(implicit O: Order[K],
                    A: Apply[Series[F, K, L, ?]]): Series[F, K, L, V] =
    self <* Index(Seq(key))
   */

  def covary[F2[a]](implicit ev: F[_] =:= Pure[_]): Series[F2, K, L, V] =
    self.asInstanceOf[Series[F2, K, L, V]]

  private def isOutOfOrder(implicit O: PartialOrder[K]) =
    if (isUnique)(a: K, b: K) => a >= b
    else (a: K, b: K) => a > b

  /** Adds assertions to check `isUnique` and `isOrdered` at runtime.
    * Use this if you can't guarantee that your source is ordered and/or
    * unique, but you expect it to be.
    * Note that this may have a performance (for ordering) or space
    * (for unique) penalty. Checking uniqueness particularly requires
    * unbounded space.
    * TODO: wrong - you only need to maintain the last element and check that ordering increases.
    */
  def safe(
      implicit
      F: MonadError[F, Throwable],
      O: PartialOrder[K]
  ): Series[F, K, L, V] =
    new Series(
      (isUnique, isOrdered) match {
        case (true, false) => //TODO : consider making Set mutable
          toStream.mapAccumulate(F pure Set.empty[K]) {
            case (fs, kv @ (k, v)) =>
              (fs flatMap { set =>
                if (set contains k) F raiseError Series.NonUniqueException(kv)
                else F pure (set + k)
              }, kv)
          } evalMap {
            case (fs, kv) =>
              fs as kv
          }
        case (_, true) =>
          toStream.zipWithPrevious.evalMap {
            case (Some(a), b) =>
              if (isOutOfOrder(O)(a._1, b._1))
                F.raiseError[(K, V)](Series.OutOfOrderException(a, b))
              else F pure b
            case (None, kv) => F pure kv
          }
        case _ => toStream //TODO : you probably didn't mean to do this ...
      },
      isUnique,
      isOrdered
    )

  def take(n: Int): Series[F, K, L, V] =
    new Series(toStream take n, isUnique, isOrdered)

  def takeRight(n: Int): Series[F, K, L, V] =
    new Series(toStream takeRight n, isUnique, isOrdered)

  def head: Series[F, K, L, V] =
    new Series(toStream take 1, isUnique, isOrdered)

  def tail: Series[F, K, L, V] =
    new Series(toStream.tail, isUnique, isOrdered)

  def init: Series[F, K, L, V] =
    new Series(toStream drop 1, isUnique, isOrdered)

  //This implementation is gross but needed to maintain chunk structure as much as possible.
  /** The series with unique keys.
    * If the series index is not already sorted, it will be reordered.
    * @param keepFirst Determine whether to keep the first or last value and
    *                  drop the others.
    */
  def unique(keepFirst: Boolean = true)(
      implicit O: Order[K]): Series[F, K, L, V] =
    if (isUnique) self
    else if (!isOrdered) ordered.unique(keepFirst)
    else
      new Series(
        {
          // if k+1 > k, output k.
          def eager(fkv: Stream[F, (K, V)])(
              prev: Option[K]): Pull[F, (K, V), Unit] =
            fkv.pull.uncons.flatMap {
              case Some((kvs @ (ks |: vs), kvs2)) =>
                var k = prev
                var i = 0
                while (i < ks.size && (k.isEmpty || ks(i) > k.get)) {
                  k = Some(ks(i))
                  i += 1
                }

                var j = 0
                while (j < ks.size && (prev.nonEmpty && ks(j) == prev.get)) j += 1
                assert(i == 0 || j == 0) //the two loops above are mutually exclusive
                //System.err.println(s"prev: $prev, kvs: $kvs, i: $i, j: $j")
                (Pull.output(kvs take i) >>
                  eager(kvs2 consChunk (kvs drop i + j))(k))
              case None =>
                Pull.done
            }

          // if k+1 > k, output k-1.
          def lzy(fkv: Stream[F, (K, V)])(
              prev: Option[(K, V)]): Pull[F, (K, V), Unit] =
            fkv.pull.uncons.flatMap {
              case None =>
                prev.map(Pull.output1).getOrElse(Pull.pure({})) >> Pull.done
              case Some((kvs, kvs2)) if kvs.isEmpty =>
                lzy(kvs2)(prev)
              case Some((kvs @ (ks |: vs), kvs2)) =>
                if (prev.isEmpty)
                  lzy(kvs2 consChunk (kvs drop 1))(Some(kvs(0)))
                else {
                  assert(!prev.isEmpty) //prev has not been emitted

                  var i = 0
                  var k = prev.get._1 //the prev element, including carry
                  while (i < kvs.size && k != ks(i)) { k = ks(i); i += 1 }

                  var j = 0
                  k = prev.get._1
                  while (j < kvs.size && k == ks(j)) j += 1

                  //println(s"prev: $prev, chunk: $kvs, i: $i, j: $j")
                  assert(i == 0 || j == 0)
                  if (i > 0)
                    Pull.output1(prev.get) >> Pull
                      .output(kvs take i - 1) >> lzy {
                      kvs2 consChunk (kvs drop i)
                    }(Some(kvs(i - 1)))
                  else
                    lzy(kvs2 consChunk (kvs drop j - 1))(None)
                }
            }
          if (keepFirst) eager(toStream)(None).stream
          else lzy(toStream)(None).stream
        },
        isOrdered = true,
        isUnique = true
      )

  /** Resamples the keys. This method takes a function that is iterated
    * over the [[tail]]s of this series. The input to the function will
    * be all elements from the current point onwards.
    * @example {{{
    * }}}
    */
  //def resample[K2](f: K => K2)(implicit K =:= Timestamp[_])

  /** Applies a function across rows. */
  def map[V2](f: V => V2): Series[F, K, L, V2] =
    new Series(toStream.mapChunks { case k |: v => k |: v.map(f) },
               isUnique,
               isOrdered)

  //TODO: I think this needs StepLeg for resource safety.
  /** Set intersection on keys (right associative). */
  def |:[B, M](left: Series[F, K, M, B])(
      implicit O: Order[K],
      U: Series.Union[M, L]): Series[F, K, U.Out, (B, V)] = {
    def intersect[A, B](fa: Stream[F, (K, A)],
                        fb: Stream[F, (K, B)]): Pull[F, (K, (A, B)), Unit] =
      (fa.pull.uncons product fb.pull.uncons) flatMap {
        case (None, _) | (_, None) =>
          Pull.done
        case (Some((a, as)), Some((b, bs))) if a.isEmpty || b.isEmpty =>
          intersect(if (a.isEmpty) as else as consChunk a,
                    if (b.isEmpty) bs else bs consChunk b)
        case (Some((a @ (ak |: av), as)), Some((b @ (bk |: bv), bs))) =>
          val stop = a.size min b.size
          var i = 0
          while (i < stop && ak(i) == bk(i)) i += 1
          (Pull output (ak |: av |: bv take i)) >> {
            var j, k = i
            while (j < stop && ak(j) < bk(i)) j += 1 //drop left
            while (k < stop && ak(i) > bk(k)) k += 1 //drop right
            intersect(as consChunk (a drop j), bs consChunk (b drop k))
          }
      }

    new Series(
      toStream = intersect(left.ordered.toStream, self.ordered.toStream).stream,
      isOrdered = true,
      isUnique = isUnique & left.isUnique,
    ).labelled[U.Out]
  }

  /** Sorts the series by keys.
    * @note This blocks streaming (unsorted) series.
    * @todo Use a sorting algorithm that is biased for already-sorted streams.
    */
  def ordered(implicit O: Order[K]): Series[F, K, L, V] =
    if (isOrdered) self
    else
      new Series(
        toStream.chunks
          .fold(Vector.empty[(K, V)])(_ ++ _.toVector)
          .flatMap { kvs =>
            val (ks, vs) = kvs
              .sorted(Ordering.by[(K, V), K](_._1)(O.toOrdering))
              .unzip
            Stream.chunk(Chunk.vector(ks) |: Chunk.vector(vs)).covary[F]
          },
        isUnique = self.isUnique,
        isOrdered = true
      )

  /** @see cats.Show */
  override def toString =
    s"Series(isUnique = $isUnique, isOrdered = $isOrdered)"
}

object Series
    extends Labels
    with cats.syntax.ShowSyntax
    with cats.syntax.EqSyntax
    with cats.syntax.PartialOrderSyntax
    with cats.syntax.FunctorSyntax
    with cats.syntax.SemigroupalSyntax
    with cats.syntax.ApplySyntax {

  private[timeseries] def isSeqUnique[C[a] <: Seq[a], A](c: C[A]): Boolean =
    c.groupBy(Predef.identity).values.forall(_.length == 1)

  def empty[K] =
    new Series[Pure, K, Nothing, Nothing](
      toStream = Stream.empty.covaryOutput[(K, Nothing)],
      isUnique = true,
      isOrdered = true
    )

  /** Specialized constructor for `Series` backed by column-major arrays.
    * </p> If doing interactive computing, it is recommended to avoid `Array`s,
    * as they are mutable. Also this constructor requires both arguments to
    * be `Array`s, whereas more flexible `Seq` constructor allows mixing
    * collections for the index and values.
    */
  def apply[K: Order, V](
      index: Array[K],
      values: Array[V],
      unique: Option[Boolean],
      sorted: Option[Boolean]
  ): Series[Pure, K, Nothing, V] = {
    Series[ArraySeq, ArraySeq, K, V](
      ArraySeq.unsafeWrapArray[K](index),
      ArraySeq.unsafeWrapArray[V](values),
      unique,
      sorted
    )
  }

  /** Creates a Series form a single key/value pair.
    * TODO : change effect to cats.Id
    */
  def apply[K: Order, V](pair: (K, V)): Series[Pure, K, Nothing, V] =
    new Series(Stream emit pair, isUnique = true, isOrdered = true)

  def apply[K: Order, V](
      kv: (K, V),
      kv2: (K, V),
      kvs: (K, V)*
  ): Series[Pure, K, Nothing, V] = {
    val (ks, vs) = (kv +: kv2 +: kvs.toSeq).unzip
    Series(ks, vs)
  }

  /** Takes a `fs2.Stream` of `values` and timestamps them in `τ`s.
    * @usecase def timestamp[τ](values : Stream[F,V], unique : Boolean = false)(implicit timer : Timer[F]) : TimeSeries[F, τ, L, V]
    * @param unique True if the rate of emission is greater than `τ`.
    */
  def timestamp[τ <: TimeUnit] = new TimestampedBuilder[τ] {}

  protected trait TimestampedBuilder[τ <: TimeUnit] {

    def apply[F[_]: Functor: Timer, V](
        values: Stream[F, V],
        unique: Boolean = false
    )(implicit T: TimestampConversion[τ]): TimeSeries[F, τ, Nothing, V] =
      new Series(
        values evalMap (v => Timestamp[τ].fromTimer[F] map (_ -> v)),
        unique,
        true
      )
  }

  /** A `Series` backed by column-major `Seq`s. This is the canonical
    * constructor, meant for interactive computing.
    * `Vector` is  the recommended choice for collection as it provides good
    * insertion and lookup performance.
    * @example {{{
    * scala> Series(1 to 3, Seq.fill(3)(math.random))
    * res0: timeseries.Series[fs2.Pure,Int,Nothing,Double] = Series(isUnique = true, isOrdered = true)
    *
    * scala> res0.show
    * res1: String =
    * |           1 |      0.3393 |
    * |           2 |      0.3470 |
    * |           3 |      0.2265 |
    * }}}
    */
  def apply[C[a] <: Seq[a], D[a] <: Seq[a], K: Order, V](
      index: C[K],
      values: D[V],
      unique: Option[Boolean] = None,
      sorted: Option[Boolean] = None
  ): Series[Pure, K, Nothing, V] = {
    assert(
      index.length == values.length,
      "Index and values must have same length (" + index.length + " vs " + values.length + ")"
    )

    import scala.reflect._
    def colChunk[F[_], O](coll: F[O]): Chunk[O] = {
      coll match {
        case c: Vector[_]     => Chunk vector c
        case c: ArraySeq[_]   => Chunk array c.unsafeArray
        case c: IndexedSeq[_] => Chunk indexedSeq c
        case c: Seq[_]        => Chunk seq c
      }
    }.asInstanceOf[Chunk[O]]

    val toStream = Stream chunk (colChunk(index) |: colChunk(values))

    val isUnique = unique getOrElse isSeqUnique(index)

    val isOrdered = sorted getOrElse {
      index.sorted(Order[K].toOrdering) sameElements index
    }

    new Series[Pure, K, Nothing, V](toStream, isUnique, isOrdered)
  }

  /** Indicates a data point is received out of order.
    * @param offending The object that was received out-of-order.
    * @param before    The object that was received immediately before.
    */
  case class OutOfOrderException[K](offending: K, before: K)
      extends IllegalStateException(
        s"$offending was received after $before in an ordered series."
      )

  /** Indicates a datapoint appears more than once in a series that has been
    * designated unique by the originator. */
  case class NonUniqueException[K](dupe: K)
      extends IllegalStateException(s"$dupe is duplicated in a unique series.")

  /** Returns a random `Series`.
    * The result of this will depend on which `series.syntax._` you've imported.
    */
  def random[F[_], K: Arbitrary: Order, V: Arbitrary](
      implicit
      A: Arbitrary[Series[F, K, Nothing, V]]
  ): Series[F, K, Nothing, V] =
    A.arbitrary.pureApply(Gen.Parameters.default, rng.Seed.random())

  implicit def covaryPure[F[_], L, K, V](
      fkv: Series[Pure, K, L, V]
  ): Series[F, K, L, V] =
    fkv.covary[F]

  implicit def indexSyntax[F[_], L, K, V](s: Series[F, K, L, V]) = new {

    def index(implicit tc: FlatMap[Series[F, ?, L, V]]) =
      FlatMap.ops.toAllFlatMapOps[Series[F, ?, L, V], K](s)(tc)
  }
}
