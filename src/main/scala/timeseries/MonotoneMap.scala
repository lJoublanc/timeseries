package timeseries

import fs2.Fallible
import cats.{PartialOrder, Monoid, Id}
import cats.arrow.Arrow
import org.scalacheck.Arbitrary

/** Adds context to a map `F : A → B` to determine <b>if</b> it strict.
  * You can check whether this is the case using `isStrict`.
  * </p> A monotone map `F` is one which preserves ordering. i.e.:
  * `F(a) ≤ F(b) => a ≤ b`. [[timeseries.MonotoneFn.arrowInstance]] provides
  * identity and composition methods.
  * @tparam G The context of the codomain; allows compatiblity with
  *           `cats.data.Kleisli` etc.
  * @tparam A The domain.
  * @tparam B The co-domain.
  */
trait MonotoneMap[G[_], A, B] extends cats.syntax.ArrowSyntax {

  def apply(a: A): G[B]

  /** True iff `f(a + ε) > f(a) for all positive ε`. */
  def isStrict: Boolean
}

/** This class extends `scala.Function1` so that you can pass around
  * `MonotoneFn`s wherever you would use a `Function1`.
  * @see [[timeseries.implicits.MonotoneFnBuilder]]
  */
class MonotoneFn[A, B](f: A => B, val isStrict: Boolean)
    extends Function1[A, B]
    with MonotoneMap[Id, A, B] {
  def apply(a: A) = f(a)
}

object MonotoneFn {
  import MonotoneMap.{MaxSamplesExceeded, CounterExample}

  /** Lift a function into `F` unsafely, specifying whether it is strict
    * monotonic or not.
    */
  def unsafe[A, B](f: A => B, isStrict: Boolean): MonotoneFn[A, B] =
    new MonotoneFn(f, isStrict)

  /** Given the domain and codomain order, lifts `F` into a monotonic one.
    * At runtime, this method will use `Arbitrary[A]` to draw a number of
    * random `a`s and calculate the value `f(a)` to ensure that the function is,
    * in fact, monotonic.
    * TODO : This is done using `assert`, so is `elidable` if performance is an issue. In most cases this will have a little bit of overhead,
    * but provide some sanity checks.
    * @param f              The function to be lifted to a monotonic function.
    * @param samples        Number of times to sample the function.
    * @throws timeseries.MonotoneMap.CounterExample if function is not monotonic.
    * @note This method will sample the funciton `f`, therefore it <em>must be
    *       pure</em>.
    */
  def apply[A: PartialOrder: Arbitrary, B: PartialOrder](
      f: A => B,
      samples: Int = 100
  ): A =>> B = {
    val isStrictMonotone = {
      import fs2.Stream
      import cats.effect.IO
      import cats.syntax.all._
      import cats.kernel.Comparison._
      import cats.instances.option._
      import cats.instances.either._
      import org.scalacheck.Arbitrary.arbitrary
      require(samples <= MaxSamplesExceeded.value)

      val gen = arbitrary[(A, A)]

      Stream
        .repeatEval(IO(gen.sample))
        .evalMap { optab =>
          IO {
            optab flatMap {
              case (a, b) =>
                import math._
                PartialOrder[A].partialComparison(a, b) product PartialOrder[B]
                  .partialComparison(f(a), f(b)) map {
                  case (LessThan, LessThan) | (GreaterThan, GreaterThan) |
                      (EqualTo, EqualTo) =>
                    true
                  case (LessThan | GreaterThan, EqualTo) => false
                  case _ =>
                    val e = CounterExample(a, b, f(a), f(b))
                    System.err.println(e)
                    throw e
                }
            }
          }
        }
        .take(MaxSamplesExceeded.value)
        .append(Stream.raiseError[IO](MaxSamplesExceeded))
        .unNone
        .take(samples)
        .compile
        .fold(true)(_ && _)
        .unsafeRunSync
    }
    new MonotoneFn(f, isStrictMonotone)
  }

  implicit def arrowInstance = new Arrow[MonotoneFn] {
    import cats.implicits._

    /** Casts `f` into a monotone function.
      * @throws ClassCastException.
      */
    def lift[A, B](f: A => B): A =>> B = f match {
      case f: (A =>> B) => f
      case f: (A => B) =>
        throw new ClassCastException(
          "`f` is not of type `=>>`. Use `MonotoneFn.unsafe`" +
            " or `MonotoneFn.apply` to create one."
        )
    }

    def compose[A, B, C](f: B =>> C, g: A =>> B): A =>> C =
      new MonotoneFn(f compose g, g.isStrict && f.isStrict)

    def first[A, B, C](fa: A =>> B): (A, C) =>> (B, C) =
      new MonotoneFn[(A, C), (B, C)](
        tup => (fa(tup._1), tup._2),
        fa.isStrict
      )
  }
}

case class MonotoneSeries[F[_], L, K: cats.Order, V: cats.Order](
    s: Series[F, L, K, V])(implicit A: cats.Apply[Series[F, L, K, ?]])
    extends MonotoneMap[Series[F, L, K, ?], K, V] {

  require(s.isOrdered)

  def apply(key: K) = ??? //s(key)

  def isStrict = s.isUnique
}

object MonotoneMap {
  import org.scalacheck.Arbitrary
  import org.scalacheck.Prop.forAll

  case class CounterExample[A, B](a1: A, a2: A, b1: B, b2: B)
      extends Throwable {
    override def toString: String =
      s"CounterExample : f is not monotonic for f($a1) = $b1, f($a2) = $b2."
  }

  case object MaxSamplesExceeded extends Throwable {
    val value = 10000
    override def toString =
      s"Was unable to draw enough samples, stopped at $value"
  }
}
