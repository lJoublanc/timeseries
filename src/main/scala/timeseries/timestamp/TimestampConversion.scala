package timeseries.timestamp

/** Provide conversions to/from standard Scala and Java types. */
trait TimestampConversion[τ <: TimeUnit] extends AnyRef {

  def toInstant(t: Timestamp[τ]): java.time.Instant

  def fromInstant(t: java.time.Instant): Timestamp[τ]

  def toDuration(t: Timestamp[τ]): scala.concurrent.duration.FiniteDuration

  def toJavaTimeUnit: scala.concurrent.duration.TimeUnit
}

object TimestampConversion {
  def apply[τ <: TimeUnit](implicit t: TimestampConversion[τ]) = t
}
