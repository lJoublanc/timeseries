package timeseries

import java.time.Instant
import scala.concurrent.duration.Duration
import scala.concurrent.duration.{
  NANOSECONDS,
  MICROSECONDS,
  MILLISECONDS,
  SECONDS,
  MINUTES,
  HOURS,
  DAYS,
  DurationLong
}

import cats.syntax.functor._
import cats.syntax.contravariant._
import cats.{Order, Show, Functor, Contravariant}
import org.scalacheck.{Arbitrary, Gen}

package object timestamp {

  /** Timestamp value class, modelled after [[https://github.com/numpy/numpy/blob/c90d7c94fd2077d0beca48fa89a423da2b0bb663/doc/neps/datetime-proposal.rst NumPy proposal]].
    * Coincidentally this overlaps with the encoding used in the FPL FAST
    * specification, extension 1.2.
    *
    * Only a subset of the values are supported, i.e. those corresponding to
    * `java.util.concurrent.TimeUnit`.
    *
    * Specifically, multiple or quotient values are not currently supported
    * e.g. `5s` (5 seconds).
    *
    * @param toLong The number of τ since Unix epoch.
    */
  case class Timestamp[τ <: TimeUnit](toLong: Long)

  /** Every `scala.concurrent.duration` has the same type
    * `java.util.concurrent.TimeUnit`, being a Java enum, so we added this to
    * allow programming at the type-level.  The names correspond to the
    * abbreviated methods in `scala.concurrent.duration.DurationConversions`
    * to easily remember.
    */
  sealed trait TimeUnit

  //TODO : document the timescale used, i.e. how are leap-seconds treated.
  //TODO : add assertions to check instant is within representable bounds. See [https://github.com/numpy/numpy/blob/c90d7c94fd2077d0beca48fa89a423da2b0bb663/doc/neps/datetime-proposal.rst]
  /** @note The `java.time` timescale is different from POSIX (esp. leap-seconds), and is not guaranteed to be monotonically increasing. */
  case object Days extends TimeUnit {
    implicit val conversion = new TimestampConversion[Days] {

      def toInstant(t: Timestamp[Days]) =
        Instant ofEpochSecond (t.toLong * 60L * 60L * 24L)

      def fromInstant(t: Instant): Timestamp[Days] = ???

      def toDuration(t: Timestamp[Days]) = t.toLong days

      def toJavaTimeUnit = DAYS
    }
  }

  type Days = Days.type

  case object Hours extends TimeUnit {
    implicit val conversion = new TimestampConversion[Hours] {

      def toInstant(t: Timestamp[Hours]) =
        Instant ofEpochSecond (t.toLong * 60L * 60L)

      def fromInstant(t: Instant): Timestamp[Hours] =
        throw new IllegalArgumentException(
          "Hours are not FAST compliant, and are not supported and present."
        )

      def toDuration(t: Timestamp[Hours]) = t.toLong hours

      def toJavaTimeUnit = HOURS
    }
  }

  type Hours = Hours.type

  case object Minutes extends TimeUnit {
    implicit val conversion = new TimestampConversion[Minutes] {

      def toInstant(t: Timestamp[Minutes]) =
        Instant ofEpochSecond (t.toLong * 60L)

      def fromInstant(t: Instant): Timestamp[Minutes] =
        throw new IllegalArgumentException(
          "Minutes are not FAST compliant, and are not supported and present."
        )

      def toDuration(t: Timestamp[Minutes]) = t.toLong minutes

      def toJavaTimeUnit = MINUTES
    }
  }

  type Minutes = Minutes.type

  case object Seconds extends TimeUnit {
    implicit val conversion = new TimestampConversion[Seconds] {

      def toInstant(t: Timestamp[Seconds]) = Instant ofEpochSecond t.toLong

      def fromInstant(t: Instant) = Timestamp[Seconds](t.getEpochSecond)

      def toDuration(t: Timestamp[Seconds]) = t.toLong seconds

      def toJavaTimeUnit = SECONDS
    }
  }

  type Seconds = Seconds.type

  case object Millis extends TimeUnit {
    implicit val conversion = new TimestampConversion[Millis] {
      import scala.concurrent.duration._

      def toInstant(t: Timestamp[Millis]) = Instant ofEpochMilli t.toLong

      def fromInstant(t: Instant) = Timestamp[Millis](
        t.getEpochSecond * 1000L + (t.getNano / 1000000).toLong
      )

      def toDuration(t: Timestamp[Millis]) = t.toLong millis

      def toJavaTimeUnit = MILLISECONDS
    }
  }

  type Millis = Millis.type

  case object Micros extends TimeUnit {
    implicit val conversion = new TimestampConversion[Micros] {
      import scala.concurrent.duration._

      def toInstant(t: Timestamp[Micros]) =
        Instant ofEpochSecond (t.toLong / 1000000L, t.toLong % 1000000L)

      def fromInstant(t: Instant) = Timestamp[Micros](
        t.getEpochSecond * 1000000L + (t.getNano / 1000).toLong
      )

      def toDuration(t: Timestamp[Micros]) = t.toLong micros

      def toJavaTimeUnit = MICROSECONDS
    }
  }

  type Micros = Micros.type

  case object Nanos extends TimeUnit {
    implicit val conversion = new TimestampConversion[Nanos] {

      def toInstant(t: Timestamp[Nanos]) =
        Instant ofEpochSecond (t.toLong / 1000000000L, t.toLong % 1000000000L)

      def fromInstant(t: Instant) =
        Timestamp[Nanos](t.getEpochSecond * 1000000000L + t.getNano.toLong)

      def toDuration(t: Timestamp[Nanos]) = t.toLong nanos

      def toJavaTimeUnit = NANOSECONDS
    }
  }

  type Nanos = Nanos.type

  object Timestamp extends cats.syntax.ShowSyntax {

    def apply[τ <: TimeUnit] = new TimestampBuilder[τ] {}

    implicit def syntax[τ <: TimeUnit](
        t: Timestamp[τ]
    )(implicit
      tc: TimestampConversion[τ]) =
      new {
        def toInstant = tc toInstant t
        def toDuration = tc toDuration t
      }

    trait TimestampBuilder[τ <: TimeUnit] extends AnyRef {
      import cats.effect.Timer
      import scala.concurrent.duration.{TimeUnit => JTimeUnit}
      import timeseries.timestamp.{TimestampConversion => TConv}

      def fromTimer[F[_]: Functor](
          implicit
          timer: Timer[F],
          T: TConv[τ]
      ): F[Timestamp[τ]] =
        timer.clock.monotonic(T.toJavaTimeUnit).map(Timestamp[τ])

      def fromInstant(t: Instant)(implicit T: TConv[τ]): Timestamp[τ] =
        T fromInstant t

      /** @return a duration object with the corresponding Java (Scala)
        *         <em>value</em> of `τ`, and <em>type</em> `TimeUnit`.
        */
      def toJavaTimeUnit(implicit T: TConv[τ]): JTimeUnit = T.toJavaTimeUnit
    }

    implicit def catsStdOrderInstance[τ <: TimeUnit]: Order[Timestamp[τ]] =
      (x, y) =>
        cats.instances.long.catsKernelStdOrderForLong
          .compare(x.toLong, y.toLong)

    //TODO : val ts : StringInterpolator

    /** Default `show` in ISO format. Can be heavy as it allocates a
      * `java.time.Instant` object
      */
    implicit def showInstant[τ <: TimeUnit](
        implicit T: TimestampConversion[τ]): Show[Timestamp[τ]] =
      Show.fromToString[java.time.Instant] contramap (T.toInstant)

    implicit def arbitraryInstance[τ <: TimeUnit](
        implicit arb: Arbitrary[Long]): Arbitrary[Timestamp[τ]] =
      Arbitrary { arb.arbitrary map Timestamp[τ] }
  }
}
