package timeseries

import scala.collection.immutable.NumericRange

import cats.{Show, Eq, PartialOrder, Order}
import org.scalacheck.{Arbitrary, Gen}
import cats.syntax.show._

/** Provides default instances for types not part of the `timeseries` package. */
package object implicits {

  /** Display columns of data. They may be defined recursively.
    * @tparam K The head column. Can be a tuple.
    * @tparam V The tail column(s). Can be a tuple.
    */
  implicit def showRow[K: Show, V: Show]: Show[(K, V)] =
    _ match {
      case (k, v) => k.show + " | " + v.show
    }

  /** Display singleton values.
    * We use `Singleton` instead of `ValueOf` for 2.12 compat.
    */
  implicit def showForSingletonShow[S <: Singleton]: Show[S] =
    Show.fromToString[S]

  /** Aligned to max `Long` digits. */
  implicit def showForLong: Show[Long] = s => f"$s% 20d"

  /** Aligned to max `Int` digits. */
  implicit def showForInt: Show[Int] = s => f"$s% 11d"

  /** Aligned to four decimals and max `Double` digits. */
  implicit def showForDouble: Show[Double] = s => f"$s% 11.4g"

  /** Aligned to four decimals and max `Float` digits. */
  implicit def showForFloat: Show[Float] = s => f"$s% 10.4g"

  /** This is required for [[timeseries.Index]]. */
  implicit def showForNothing: Show[Nothing] = _ => ""

  /** Displays the symbolic representation. */
  implicit def showForChar: Show[Char] =
    Show.fromToString[Char]

  implicit def orderForInt: Order[Int] =
    cats.instances.int.catsKernelStdOrderForInt

  implicit def orderForLong: Order[Long] =
    cats.instances.long.catsKernelStdOrderForLong

  implicit def eqForDouble: Eq[Double] =
    cats.instances.double.catsKernelStdOrderForDouble

  implicit def eqForFloat: Eq[Float] =
    cats.instances.float.catsKernelStdOrderForFloat

  /** Provides postfix syntax for instantiating monotone functions.
    * @example {{{
    * import timeseries.implicits._
    * import cats.instances.double._
    * import math._
    * { x : Double => sqrt(x) } ∀ { x => x > 0 }
    * { x : Double => sin(x) } ∀ { x => x >= 0 && x <= Pi }
    * { x : Int => x * x } ∈ NumericRange(0,10000,1)
    * }}}
    */
  implicit class MonotoneFnBuilder[X, Y](
      f: X => Y
  )(implicit
    domainOrder: PartialOrder[X],
    codomainOrder: PartialOrder[Y]) {

    def forall(x: X => Boolean)(implicit X: Arbitrary[X]): X =>> Y = {
      val domain = Arbitrary(X.arbitrary filter x) //FIXME : this fails for small ranges e.g. 0 < x < 2π.
      MonotoneFn[X, Y](f)(domainOrder, domain, codomainOrder)
    }

    @inline def ∀(x: X => Boolean)(implicit X: Arbitrary[X]) = forall(x)

    def forelem(
        x: NumericRange[X] //FIXME : specifying a step != 1 will fail.
    )(implicit
      n: Numeric[X],
      c: Gen.Choose[X]): X =>> Y = {
      val domain = Arbitrary(Gen.choose(x.start, x.end))
      MonotoneFn[X, Y](f)(domainOrder, domain, codomainOrder)
    }

    @inline def ∈(
        x: NumericRange[X]
    )(implicit
      n: Numeric[X],
      c: Gen.Choose[X]) = forelem(x)
  }
}
