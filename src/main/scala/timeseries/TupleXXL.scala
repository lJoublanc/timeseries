package timeseries

import annotation.implicitNotFound

import cats.Semigroup
import cats.arrow.FunctionK
import cats.syntax.semigroup._

/** Generics on right-nested tuples. Most of these are named/implemented
  * after the same operators in Shapeless.
  */
protected trait TupleXXL {

  /** For dotty compat.
    * @todo: Move into a 2.x specific src to avoid clash
    */
  type *:[A, B] = (A, B)

  implicit class DottyTupleXXLCons[A](val right: A) {
    def *:[B](left: B): B *: A = (left, right)
  }

  trait LiftAll[F[_], A] {
    type Out

    /** Return a tuple with the typeclass `F[_]` for each element. */
    def lift: Out
  }

  object LiftAll {

    type Aux[F[_], A, FA] = LiftAll[F, A] { type Out = FA }

    def apply[F[_], A](implicit fa: LiftAll[F, A]): LiftAll.Aux[F, A, fa.Out] =
      fa

    implicit def nil[F[_]]: LiftAll.Aux[F, Unit, Unit] =
      new LiftAll[F, Unit] {
        type Out = Unit
        def lift = ()
      }

    implicit def uncons[F[_], A, Bs, FBs](
        implicit @implicitNotFound("Could not find a ${F} instance for ${A}.")
        fa: F[A],
        tail: LiftAll.Aux[F, Bs, FBs]): LiftAll.Aux[F, A *: Bs, F[A] *: FBs] =
      new LiftAll[F, A *: Bs] {
        type Out = F[A] *: FBs
        def lift = (fa, tail.lift)
      }
  }

  trait Zip[A, B] {
    type Out // <: (_,_) //strange - when uncommenting, compiler crashes.
    /** Elements of `A` and `B` zipped together. */
    def apply(a: A, b: B): Out
  }

  object Zip {

    type Aux[A, B, AB] = Zip[A, B] { type Out = AB }

    def apply[A, B](a: A, b: B)(implicit z: Zip[A, B]): z.Out = z(a, b)

    implicit lazy val nil: Zip.Aux[Unit, Unit, Unit] =
      new Zip[Unit, Unit] {
        type Out = Unit
        def apply(a: Unit, b: Unit) = {}
      }

    implicit def uncons[A1, As, B1, Bs, ABs](
        implicit tail: Zip.Aux[As, Bs, ABs])
      : Zip.Aux[A1 *: As, B1 *: Bs, (A1, B1) *: ABs] =
      new Zip[A1 *: As, B1 *: Bs] {
        type Out = (A1, B1) *: ABs
        def apply(a: (A1, As), b: (B1, Bs)) =
          ((a._1, b._1), tail.apply(a._2, b._2))
      }
  }

  /** Dual of [[Zip]] */
  trait Unzip[As] {
    type Out
    def apply(as: As): Out
  }

  object Unzip {
    type Aux[As, T] = Unzip[As] {
      type Out = T
    }

    def apply[LRs, Ls, Rs](zipped: LRs)(
        implicit unzip: Unzip.Aux[LRs, (Ls, Rs)]): (Ls, Rs) =
      unzip(zipped)

    implicit def one[L, R]: Unzip.Aux[(L, R) *: Unit, (L *: Unit, R *: Unit)] =
      new Unzip[(L, R) *: Unit] {
        type Out = (L *: Unit, R *: Unit)
        def apply(zipped: (L, R) *: Unit) = zipped match {
          case ((l, r), ()) => (l *: (), r *: ())
        }
      }

    implicit def uncons[L, R, LRs, Ls, Rs](
        implicit unzip: Unzip.Aux[LRs, (Ls, Rs)])
      : Unzip.Aux[(L, R) *: LRs, (L *: Ls, R *: Rs)] =
      new Unzip[(L, R) *: LRs] {
        type Out = (L *: Ls, R *: Rs)
        def apply(zipped: (L, R) *: LRs) = zipped match {
          case ((l, r), lrs) =>
            unzip(lrs) match {
              case (ls, rs) => (l *: ls, r *: rs)
            }
        }
      }
  }
  @implicitNotFound("${As} does not contain an element of type ${A}")
  trait Reinsert[A, As] {
    type Out

    /** Reinsert the first `A` at the front of `As` */
    def apply(as: As): Out
  }

  trait LowPrioReinsert {

    implicit def uncons[A, B, Bs, ABs](
        implicit reinsert: Reinsert.Aux[A, ABs, A *: Bs])
      : Reinsert.Aux[A, B *: ABs, A *: B *: Bs] =
      new Reinsert[A, B *: ABs] {
        type Out = A *: B *: Bs
        def apply(babs: B *: ABs) = babs match {
          case (b, abs) =>
            reinsert(abs) match {
              case (a, bs) => a *: b *: bs
            }
        }
      }
  }

  object Reinsert extends LowPrioReinsert {

    type Aux[A, As, Bs] = Reinsert[A, As] { type Out = Bs }

    def apply[A] = new {
      def apply[ABs, Bs](abs: ABs)(
          implicit reinsert: Reinsert.Aux[A, ABs, A *: Bs]): A *: Bs =
        reinsert(abs)
    }

    implicit def id[A, As]: Reinsert.Aux[A, A *: As, A *: As] =
      new Reinsert[A, A *: As] {
        type Out = A *: As
        def apply(aas: A *: As) = aas
      }
  }

  @implicitNotFound("No ${X} found in {AXs}")
  trait Drop[X, AXs] {
    type Out

    /** Return `AXs` without the first `X`.
      * This is simply [[Reinsert]] discarding the head value.
      */
    def apply(axs: AXs): Out
  }

  object Drop {
    type Aux[X, AXs, As] = Drop[X, AXs] { type Out = As }

    def apply[X] = new {
      def apply[AXs, As](axs: AXs)(implicit drop: Drop.Aux[X, AXs, As]): As =
        drop(axs)
    }

    implicit def instance[X, AXs, As](
        implicit reinsert: Reinsert.Aux[X, AXs, (X, As)])
      : Drop.Aux[X, AXs, As] =
      new Drop[X, AXs] {
        type Out = As
        def apply(axs: AXs): As = reinsert(axs)._2
      }
  }

  @implicitNotFound("No ${A} found in ${ABs}")
  trait Find[A, ABs] {
    def apply(abs: ABs): A
  }

  object Find {

    /** Return first `A` in `ABs`.
      * This is just [[Reinsert]], discarding the second element.
      */
    def apply[A] = new {
      def apply[ABs](abs: ABs)(implicit find: Find[A, ABs]): A = find(abs)
    }

    implicit def any[A, ABs, Bs](
        implicit reinsert: Reinsert.Aux[A, ABs, A *: Bs]): Find[A, ABs] =
      reinsert(_)._1
  }

  /** AKA Set/boolean algebra/field. An operation used to combine two sets. */
  sealed trait SetJoin[F[_], As, Bs] {

    /** Type-class used to transform elements in the resulting set. */
    type Combine[_]
    type Out
    def apply(as: As, bs: Bs)(f: FunctionK[F, Combine]): Out
  }

  @implicitNotFound(
    "${As} and ${Bs} do not intersect, or could not find implicit ${F} to combine each overlapping element.")
  trait Intersect[F[_], As, Bs] extends SetJoin[F, As, Bs] {

    final type Combine[x] = Semigroup[x]

    type Out

    /** Return the intersection of types `As` and `Bs`, with common values
      * combined using `cats.Semigroup`. It is assumed that both the left
      * and right set are unique in the types.
      */
    def apply(as: As, bs: Bs)(f: FunctionK[F, Semigroup]): Out
  }

  trait LowPrioIntersect {
    implicit def nil[F[_], Bs]: Intersect.Aux[F, Unit, Bs, Unit] =
      new Intersect[F, Unit, Bs] {
        type Out = Unit
        def apply(none: Unit, bs: Bs)(f: FunctionK[F, Semigroup]) = {}
      }

    //left head element not in right set, but futher common elements.
    implicit def inxN[F[_], A, AXs, BXs, Xs](
        implicit intersect: Intersect.Aux[F, AXs, BXs, Xs])
      : Intersect.Aux[F, A *: AXs, BXs, Xs] =
      new Intersect[F, A *: AXs, BXs] {
        type Out = Xs
        def apply(as: A *: AXs, bxs: BXs)(f: FunctionK[F, Semigroup]) =
          as match {
            case (a, axs) =>
              intersect(axs, bxs)(f)
          }
      }
  }

  object Intersect extends LowPrioIntersect {
    type Aux[F[_], As, Bs, ABs] = Intersect[F, As, Bs] { type Out = ABs }

    /** Calculate the intersection of `As` and `Bs`.  By default intersecting
      * values will be combined using `cats.Semigroup` You can override this
      * with `f` succinctly, using the kind-projector plugin.
      * @example{{{
      * import cats.{Semigroup, $tilde$greater}
      * import cats.arrow.FunctionK
      * import cats.instances.all._
      * import spire.algebra.Ring
      * import spire.std.double._
      *
      * scala> Intersect(1.0, (2, 3.0))
      * res0: Double = 4.0
      * scala> Intersect(1.0, (2, 3.0), Lambda[Ring ~> Semigroup](_.minus _))
      * res1: Double = -2.0
      * }}}
      */
    def apply[F[_], As, Bs, ABs](as: As,
                                 bs: Bs,
                                 f: FunctionK[F, Semigroup] =
                                   FunctionK.id[Semigroup])(
        implicit inx: Intersect.Aux[F, As, Bs, ABs]): ABs =
      inx(as, bs)(f)

    //left head element in right set, and futher common elements.
    implicit def inx1[F[_], X, As, BXs, Bs, Xs](
        implicit
        reinsert: Reinsert.Aux[X, BXs, X *: Bs],
        F: F[X],
        intersect: Intersect.Aux[F, As, Bs, Xs])
      : Intersect.Aux[F, X *: As, BXs, X *: Xs] =
      new Intersect[F, X *: As, BXs] {
        type Out = X *: Xs
        def apply(as: X *: As, bxs: BXs)(f: FunctionK[F, Semigroup]) =
          as match {
            case (x1, as) =>
              reinsert(bxs) match { //re-arranges x as the head element.
                case (x2, bs) =>
                  val xs = intersect(as, bs)(f)
                  (f(F) combine (x1, x2), xs)
              }
          }
      }
  }
}

object TupleXXL extends TupleXXL
