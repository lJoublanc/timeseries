import fs2.{Stream, Pure}
import cats.{Order, Alternative}
import timeseries.timestamp.{Timestamp, TimeUnit}

package object timeseries {

  /** Alias for `MonotoneFn` */
  type =>>[A, B] = MonotoneFn[A, B]

  //TODO : consider whether we need the column name here ...
  /** A series with timestamps of granularity `τ` in the index */
  type TimeSeries[F[_], τ <: TimeUnit, L, V] =
    Series[F, Timestamp[τ], L, V]

  /** A series which only has an index but no values.  This is useful for
    * filtering the index of a series together with `*>` or `<*`.
    */
  type Index[F[_], L, K] = Series[F, K, L, Unit] //FIXME: this breaks associativity for *>

  object Index {
    private def nUnits(n: Int): Seq[Unit] = new Seq[Unit] {
      def apply(idx: Int) = Unit
      def iterator =
        new Iterator[Unit] {
          private var i = 0
          def hasNext = i < n
          def next() = { i += 1; () }
        }
      def length = n
    }

    /* TODO: parameter clash with Series.apply
    def apply[C[k] <: Seq[k], K: cats.Order](
        keys: C[K]): Index[Pure, Nothing, K] =
      Series[C, K, Seq, Unit](keys, nUnits(keys.size))
   */
  }
}
