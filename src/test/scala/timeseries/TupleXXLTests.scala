package timeseries

import org.scalatest._
import org.scalatest.matchers._
import TupleXXL._

/* Note that using `should compile` statements doesn't appear to work here -
 * the implicit derivations are probably lacking some context to compile
 * inside the macro.
 */
class TupleXXLTests extends FlatSpec with Matchers {

  "TupleXXL" should "LiftAll" in {
    import cats.Monoid
    import cats.instances.all._

    LiftAll[Monoid, Unit].lift shouldEqual {}

    LiftAll[Monoid, Int *: String *: Unit].lift shouldEqual
      Monoid[Int] *: Monoid[String] *: ()

    LiftAll[Monoid, Int *: String *: Double *: Unit].lift shouldEqual
      Monoid[Int] *: Monoid[String] *: Monoid[Double] *: ()
  }

  it should "Zip" in {
    //Zip(1, 2.0) shouldEqual (1, 2.0)
    Zip(1 *: 2.0 *: (), "one" *: '2' *: ()) shouldEqual
      (1, "one") *: (2.0, '2') *: ()
    Zip(1 *: 2.0 *: 3L *: (), "one" *: '2' *: 3 *: ()) shouldEqual
      (1, "one") *: (2.0, '2') *: (3L, 3) *: ()
  }

  it should "Unzip" in {
    Unzip((1, "one") *: (2.0, '2') *: ()) shouldEqual
      (1 *: 2.0 *: (), "one" *: '2' *: ())
    Unzip((1, "one") *: (2.0, '2') *: (3L, 3) *: ()) shouldEqual
      (1 *: 2.0 *: 3L *: (), "one" *: '2' *: 3 *: ())
  }

  it should "Reinsert" in {
    Reinsert[Int](1 *: 2.0 *: ()) shouldEqual
      1 *: 2.0 *: ()
    Reinsert[Double](1 *: 2.0 *: ()) shouldEqual
      2.0 *: 1 *: ()
    Reinsert[Int](1 *: 2.0 *: "three" *: ()) shouldEqual
      1 *: 2.0 *: "three" *: ()
    Reinsert[Double](1 *: 2.0 *: "three" *: ()) shouldEqual
      2.0 *: 1 *: "three" *: ()
    Reinsert[String](1 *: 2.0 *: "three" *: ()) shouldEqual
      "three" *: 1 *: 2.0 *: ()
    Reinsert[Int](1 *: 2 *: "three" *: ()) shouldEqual
      1 *: 2 *: "three" *: ()
    Reinsert[String](1 *: "two" *: "three" *: ()) shouldEqual
      "two" *: 1 *: "three" *: ()
    Reinsert[Int](1.0 *: 2.0 *: 3.0 *: 4 *: ()) shouldEqual
      4 *: 1.0 *: 2.0 *: 3.0 *: ()
    """Reinsert[Char](1 *: 2.0 *: "three" *: ())""" shouldNot compile
  }

  it should "Drop" in {
    Drop[Int](1 *: ()) shouldEqual {}
    """Drop[String](1 *: ())""" shouldNot compile
    Drop[Int](1 *: 2.0 *: ()) shouldEqual 2.0 *: ()
    Drop[String](1 *: 2.0 *: "three" *: '4' *: ()) shouldEqual
      1 *: 2.0 *: '4' *: ()
  }

  it should "Find" in {
    Find[String](1 *: 2.0 *: "three" *: '4' *: ()) shouldEqual "three"
    """Find[Long](1 *: 2.0 *: "three" *: '4' *: ())""" shouldNot compile
  }

  it should "Intersect" in {
    import cats.{Semigroup, ~>}
    import cats.instances.all._
    /*
    import spire.algebra.Ring
    import spire.std.double._
    import spire.std.int._
     */
    Intersect(1 *: (), 2.0 *: 3 *: ()) shouldEqual 4 *: ()
    /*Intersect(1 *: (), 2.0 *: 3 *: (), λ[Ring ~> Semigroup](_.multiplicative)) shouldEqual 3 *: ()
    Intersect(1 *: 2.0 *: (),
              3.0 *: '4' *: (),
              λ[Ring ~> Semigroup](_.multiplicative)) shouldEqual 6.0 *: ()
     */
    Intersect(1.0 *: 2L *: 3 *: (), 1.0 *: 2 *: "three" *: ()) shouldEqual 2.0 *: 5 *: ()
    /*Intersect(1L *: 2.0 *: '3' *: (),
              1.0 *: 2 *: "three" *: (),
              λ[Ring ~> Semigroup](_.minus _)) shouldEqual 1.0 *: ()
     */
    Intersect(1 *: 2.0 *: (), 1L *: '2' *: ()) shouldEqual {}
  }
}
