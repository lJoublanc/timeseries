package timeseries

import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global
import scala.math.Numeric

import cats.effect._
import cats.effect.syntax.all._
import cats.kernel.laws.discipline.EqTests
import cats.laws.discipline.{ApplyTests, FlatMapTests}
import cats.laws.discipline.SemigroupalTests.Isomorphisms._
import cats.syntax.eq._
import cats.tests.CatsSuite
import cats.{Eq, PartialOrder, Order}
import fs2.{Stream, Pure}
import org.scalacheck.Gen.const
import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.FunSuite

class CatsSeriesSuite extends FunSuite with CatsSuite with ShowArgs {
  import timeseries.implicits._
  import timeseries.instances.poset.arbitraryTimeSeries
  import timeseries.instances.pure.{partialOrder, catsStdApplyForValues}

  implicit val timer: Timer[IO] = IO timer global

  implicit val contextShift: ContextShift[IO] = IO contextShift global

  implicit def arbMonotoneSeriesMap[K, V] =
    Arbitrary[Series[Pure, K, Nothing, V] => Series[Pure, K, Nothing, V]] {

      type S = Series[Pure, K, Nothing, V]

      Gen.oneOf(
        Gen.choose(0, 100) flatMap { n =>
          Gen.oneOf(
            const(MonotoneFn.unsafe[S, S](_ take n, false)) :| s"Series take $n",
            const(MonotoneFn.unsafe[S, S](_ takeRight n, false)) :| s"Series takeRight $n"
          )
        },
        const(MonotoneFn.unsafe[S, S](_.tail, false)) :| "Series.tail",
        const(MonotoneFn.unsafe[S, S](_.head, false)) :| "Series.head"
      ) :| "Series =>> Series"
    }

  implicit def arbMonotoneEndoFn[
      X: PartialOrder: Arbitrary: Numeric: Gen.Choose
  ] =
    Arbitrary[X => X] {
      import Numeric.Implicits._

      {
        for {
          a <- Arbitrary.arbitrary[X]
          b <- Gen.posNum[X]
          f <- const(MonotoneFn.unsafe[X, X](a + b * _, false)) :| s"$a + $b x"
        } yield f
      } // :| "X =>> X"
    }

  type T = timestamp.Seconds

  implicit val (arb, arbF) =
    (arbitraryTimeSeries[IO, T, Int] product arbitraryTimeSeries[
      IO,
      T,
      Int => Int]).unsafeRunSync

  checkAll("Poset equality laws",
           EqTests[TimeSeries[Pure, T, Nothing, Int]].eqv)

  checkAll(
    "Poset apply laws (on endofuntions)",
    ApplyTests[TimeSeries[Pure, T, Nothing, ?]].apply[Int, Int, Int]
  )

  /*
  checkAll(
    "Poset flatMap laws (on endofunctions)",
    FlatMapTests[TimeSeries[Pure, T, Nothing, ?]].apply[Int, Int, Int]
  )
 */
}
