package timeseries

import org.scalatest._
import org.scalatest.prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Gen
import cats.syntax.show._
import fs2.Pure

import timeseries.timestamp._

import implicits._

class SeriesProps
    extends PropSpec
    with Matchers
    with GeneratorDrivenPropertyChecks {

  val genKeys = Gen.containerOf[Vector, Int](Arbitrary.arbitrary[Int])

  property("Series.ordered") {
    forAll(genKeys) { ks =>
      val ft = Series(ks, 1 to ks.size)
      val f = ft.ordered
      val (k, v) = f.toStream.toVector.unzip
      k shouldBe sorted
      f.isOrdered shouldBe true
    }
  }

  property("Series.unique") {
    def testUnique(xs: Vector[Int]) = {
      val s = xs.toSet.toVector.sorted
      xs.sorted shouldEqual s
    }

    forAll(genKeys) { ks =>
      val ft = Series(ks, 1 to ks.size)
      val eager = ft.unique(true)
      val lzy = ft.unique(false)

      withClue("eager") {
        testUnique(eager.toStream.toVector.unzip._1)
        eager.isUnique shouldBe true
      }

      withClue("lazy") {
        testUnique(lzy.toStream.toVector.unzip._1)
      }

      lzy.isUnique shouldBe true

      whenever(!ft.isUnique) {
        eager should not equal lzy
      }

      withClue("num eager != num lazy") {
        eager.toStream.toVector.length shouldEqual lzy.toStream.toVector.length
      }
    }
  }
}
