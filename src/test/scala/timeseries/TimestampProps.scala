package timeseries.timestamp

import org.scalatest._
import org.scalatest.prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Gen.choose
import cats.syntax.show._

class TimestampProps
    extends PropSpec
    with Matchers
    with GeneratorDrivenPropertyChecks {

  /* Fails if we use numbers that are too large */
  implicit val arbLong: Arbitrary[Long] =
    Arbitrary { choose(-100000000L, 1000000000L) }

  property("All timestamps should be convertable to Instant and Duration") {
    forAll { t: Timestamp[Nanos] =>
      withClue("Could not conver to instant")(t.toInstant)
      withClue("Could not convert to duration")(t.toDuration)
    }
  }
}
