package timeseries

import org.scalatest._
import org.scalatest.matchers._

/* Note that using `should compile` statements doesn't appear to work here -
 * the implicit derivations are probably lacking some context to compile
 * inside the macro.
 */
class LabelUnitTests extends FlatSpec with Matchers {
  import timeseries.implicits._
  import timeseries.instances.pure._

  val l = Series(Seq(0), Seq('l'))
  val m = Series(Seq(0), Seq('m'))
  val r = Series(Seq(0), Seq('r'))
  val x = l |: m |: r
  val y = x.labelled[("left", ("middle", "right"))]

  "Labelled Series" should "have a compiling pure `show`" in {
    "y.show" should compile
  }

  it should "select left element" in {
    y["left"].show shouldEqual l.labelled["left"].show
  }

  it should "select right element" in {
    y["right"].show shouldEqual r.labelled["right"].show
  }

  it should "select middle element" in {
    y["middle"].show shouldEqual m.labelled["middle"].show
  }

  it should "select left subset" in {
    y[("left", "middle")].show shouldEqual (l |: m)
      .labelled[("left", "middle")]
      .show
  }

  it should "select right subset" in {
    y[("middle", "right")].show shouldEqual (m |: r)
      .labelled[("middle", "right")]
      .show
  }

  it should "select non-contiguous subset" in {
    y[("left", "right")].show shouldEqual (l |: r)
      .labelled[("left", "right")]
      .show
  }

  it should "select identity" in {
    y[("left", ("middle", "right"))].show shouldEqual y.show
  }

  it should "merge labels" in {
    (l.labelled["left"] |: m.labelled["middle"] |: r
      .labelled["right"]).show shouldEqual
      y.show
  }

  "Unlabelled Series" should "have a compiling pure `show`" in {
    "x.show" should compile
  }

  it should "not compile any select statements" in {
    """x["foo"]""" shouldNot compile
  }
}
