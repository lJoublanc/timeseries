package timeseries

import org.scalatest.FunSuiteLike
import org.typelevel.discipline.Laws
import org.typelevel.discipline.scalatest._

/** Use overrides the `_.toString` of pure Series, in order to allow
  * inspecting the arguments passed to scalacheck/test.
  * This is unsafe as it assumes that the input type is pure. */
private[timeseries] trait ShowArgs { self: Discipline with FunSuiteLike =>
  import org.scalacheck.util.Pretty
  override def checkAll(name: String, ruleSet: Laws#RuleSet): Unit =
    for ((id, prop) <- ruleSet.all.properties) {
      val p = prop map { res =>
        res.copy(args = res.args.map { a =>
          if (a.arg.isInstanceOf[Series[fs2.Pure @unchecked, _, _, _]])
            a.copy(
              prettyArg = Pretty {
                params =>
                  val ser = a.arg.asInstanceOf[Series[fs2.Pure, _, _, _]]
                  val elems = ser.toStream
                    .map { case (k, v) => s"$k → $v" }
                    .toVector
                    .mkString(",")
                  s"Series(isUnique= ${ser.isUnique}, isOrdered= ${ser.isOrdered}" + {
                    if (elems.isEmpty) ", (empty series))"
                    else elems.toString + ")"
                  }
              }
            )
          else a
        })
      }
      test(name + "." + id) {
        check(p)
      }
    }
}
