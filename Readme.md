# Timeseries

A library providing a pure-functional interface to streaming time-series.

Copyright (c) Dinosaur Merchant Bank 2018

Author: Luciano Joublanc

## Quickstart

Include the package in your SBT dependencies:

    resolvers += Resolver.bintrayRepo("dmbl","dinogroup")
    libraryDependencies += "com.dinogroup" %% "timeseries" % "0.1.0"

From a Scala console, try the following:

    scala> //add examples

## Features

Data analysts typically work with linear algebra packages and vectorised data
structures provided by e.g. Python [pandas](https://pandas.pydata.org/),
[NumPy](http://www.numpy.org/), the [R](https://www.r-project.org/) language,
[Matlab](https://www.mathworks.com/products/matlab.html) or OSS clone
[Octave](https://www.gnu.org/software/octave/). These are popular as they allow
transposing mathematical notation (especially linear/matrix algebra) directly
onto program code that analysts are familiar with. Vectorised data-structures
are also efficient, parallelizing computation, using libraries such as
[BLAS](http://www.netlib.org/blas/) behind the scenes. 

By contrast, writing streaming applications that ingest data in real-time and
perform computations as new data is received traditionally require a different
programming approach: writing callbacks or using some sort of a streaming
library.

This library breaches the gap between these two paradigms.

_Timeseries_ provides a DSL for writing vectorised code (modelled after pandas)
for manipulating data at the value-level. At instantiation time, the DSL can be
compiled into either a vectorised program or a streaming program. All required
to swap between the two is a single `import` statement.

### Series
  - Creating series
  - Index operations
  - Data operations

### MontoneFn (aka `=>>`)
  - Creating monotone functions (postfix `∀`).
  - Common examples of misleading monotone functions
  - Well-behaved

### Timestamp
  

## Acknowledgements

_Timeseries_ relies on 
* [fs2](https://github.com/functional-streams-for-scala/fs2) for concurrency.
* [cats](https://github.com/typelevel/cats) for category-theoretical abstractions.
* todo [spire](https://github.com/non/spire) for linear algebra abstractions.
* todo [BLAS](http://www.netlib.org/blas/) for vectorized computation.
